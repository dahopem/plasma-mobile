# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-settings package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-settings\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-14 02:17+0000\n"
"PO-Revision-Date: 2022-07-08 07:08+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.04.1\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"

#: package/contents/ui/languages.qml:20
#, kde-format
msgid "Languages"
msgstr "Jeziki"

#: package/contents/ui/languages.qml:46
#, kde-format
msgid "Apply"
msgstr "Uveljavi"

#: package/contents/ui/main.qml:20
#, kde-format
msgid "On-Screen Keyboard"
msgstr "Tipkovnica na zaslonu"

#: package/contents/ui/main.qml:43
#, kde-format
msgid "Type anything here…"
msgstr "Vtipkajte karkoli tu…"

#: package/contents/ui/main.qml:63
#, kde-format
msgid "Sound"
msgstr "Zvok"

#: package/contents/ui/main.qml:64
#, kde-format
msgid "Whether to emit a sound on keypress."
msgstr "Ali naj oddaja zvok ob pritisku tipke."

#: package/contents/ui/main.qml:73
#, kde-format
msgid "Vibration"
msgstr "Tresenje"

#: package/contents/ui/main.qml:74
#, kde-format
msgid "Whether to vibrate on keypress."
msgstr "Ali naj se zatrese ob pritisku tipke."

#: package/contents/ui/main.qml:94
#, kde-format
msgid "Check spelling of entered text"
msgstr "Preverjanje črkovanja vnesenega besedila"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Capitalize the first letter of each sentence"
msgstr "Postavi veliko začetnico na začetek vsakega stavka"

#: package/contents/ui/main.qml:112
#, kde-format
msgid "Complete current word with first suggestion when hitting space"
msgstr "Dokončaj trenutno besedo s prvim predlogom po pritisku preslednice"

#: package/contents/ui/main.qml:121
#, kde-format
msgid "Suggest potential words in word ribbon"
msgstr "Predlagaj možne besede v pasu besed"

#: package/contents/ui/main.qml:132
#, kde-format
msgid "Insert a full-stop when space is pressed twice"
msgstr "Vstavi piko ob dvakratnem pritisku preslednice"

#: package/contents/ui/main.qml:143
#, kde-format
msgid "Configure Languages"
msgstr "Nastavi jezike"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Matjaž Jeran"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "matjaz.jeran@amis.net"

#~ msgid "Virtual Keyboard"
#~ msgstr "Navidezna tipkovnica"

#~ msgid "Bhushan Shah"
#~ msgstr "Bhushan Shah"

#~ msgid "Test keyboard:"
#~ msgstr "Testna tipkovnica:"

#~ msgid "Feedback:"
#~ msgstr "Povratni odziv:"

#~ msgid "Text correction:"
#~ msgstr "Popravek besedila:"

#~ msgid "Languages:"
#~ msgstr "Jeziki:"

#~ msgid "Theme:"
#~ msgstr "Tema:"

#~ msgid "Other:"
#~ msgstr "Ostalo:"
