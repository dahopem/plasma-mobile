# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-nm package.
#
# Volkan Gezer <volkangezer@gmail.com>, 2022.
# Emir SARI <emir_sari@icloud.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-16 02:20+0000\n"
"PO-Revision-Date: 2023-03-07 20:50+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: package/contents/ui/ConnectDialog.qml:136
#, kde-format
msgid "Invalid input."
msgstr "Geçersiz giriş."

#: package/contents/ui/ConnectionItemDelegate.qml:108
#, kde-format
msgid "Connect to"
msgstr "Bağlan:"

#: package/contents/ui/main.qml:82
#, kde-format
msgid "Wi-Fi is disabled"
msgstr "Kablosuz devre dışı"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Enable"
msgstr "Etkinleştir"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Disable Wi-Fi"
msgstr "Kablosuzu devre dışı bırak"

#: package/contents/ui/main.qml:102
#, kde-format
msgid "Add Custom Connection"
msgstr "Özel Bağlantı Ekle"

#: package/contents/ui/main.qml:108
#, kde-format
msgid "Show Saved Connections"
msgstr "Kaydedilen Bağlantıları Göster"

#: package/contents/ui/NetworkSettings.qml:14
#, kde-format
msgid "Add new Connection"
msgstr "Yeni bağlantı ekle"

#: package/contents/ui/NetworkSettings.qml:35
#, kde-format
msgid "Save"
msgstr "Kaydet"

#: package/contents/ui/NetworkSettings.qml:46
#, kde-format
msgid "General"
msgstr "Genel"

#: package/contents/ui/NetworkSettings.qml:51
#, kde-format
msgid "SSID:"
msgstr "SSID:"

#: package/contents/ui/NetworkSettings.qml:60
#, kde-format
msgid "Hidden Network:"
msgstr "Gizli Ağ:"

#: package/contents/ui/NetworkSettings.qml:66
#, kde-format
msgid "Security"
msgstr "Güvenlik"

#: package/contents/ui/NetworkSettings.qml:72
#, kde-format
msgid "Security type:"
msgstr "Güvenlik türü:"

#: package/contents/ui/NetworkSettings.qml:81
#, kde-format
msgid "None"
msgstr "Yok"

#: package/contents/ui/NetworkSettings.qml:82
#, kde-format
msgid "WEP Key"
msgstr "WEP Anahtarı"

#: package/contents/ui/NetworkSettings.qml:83
#, kde-format
msgid "Dynamic WEP"
msgstr "Dinamik WEP"

#: package/contents/ui/NetworkSettings.qml:84
#, kde-format
msgid "WPA/WPA2 Personal"
msgstr "WPA/WPA2 Kişisel"

#: package/contents/ui/NetworkSettings.qml:85
#, kde-format
msgid "WPA/WPA2 Enterprise"
msgstr "WPA/WPA2 Kurumsal"

#: package/contents/ui/NetworkSettings.qml:110
#, kde-format
msgid "Password:"
msgstr "Parola:"

#: package/contents/ui/NetworkSettings.qml:118
#, kde-format
msgid "Authentication:"
msgstr "Kimlik Doğrulama:"

#: package/contents/ui/NetworkSettings.qml:121
#, kde-format
msgid "TLS"
msgstr "TLS"

#: package/contents/ui/NetworkSettings.qml:121
#, kde-format
msgid "LEAP"
msgstr "LEAP"

#: package/contents/ui/NetworkSettings.qml:121
#, kde-format
msgid "FAST"
msgstr "FAST"

#: package/contents/ui/NetworkSettings.qml:122
#, kde-format
msgid "Tunneled TLS"
msgstr "Tünellenmiş TLS"

#: package/contents/ui/NetworkSettings.qml:123
#, kde-format
msgid "Protected EAP"
msgstr "Korunan EAP"

#: package/contents/ui/NetworkSettings.qml:132
#, kde-format
msgid "IP settings"
msgstr "IP ayarları"

#: package/contents/ui/NetworkSettings.qml:138
#, kde-format
msgid "Automatic"
msgstr "Otomatik"

#: package/contents/ui/NetworkSettings.qml:138
#, kde-format
msgid "Manual"
msgstr "Elle"

#: package/contents/ui/NetworkSettings.qml:148
#, kde-format
msgid "IP Address:"
msgstr "IP Adresi:"

#: package/contents/ui/NetworkSettings.qml:160
#, kde-format
msgid "Gateway:"
msgstr "Ağ Geçidi:"

#: package/contents/ui/NetworkSettings.qml:172
#, kde-format
msgid "Network prefix length:"
msgstr "Ağ önek uzunluğu:"

#: package/contents/ui/NetworkSettings.qml:185
#, kde-format
msgid "DNS:"
msgstr "DNS:"

#: package/contents/ui/PasswordField.qml:13
#, kde-format
msgid "Password…"
msgstr "Parola…"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Emir SARI"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "emir_sari@icloud.com"

#~ msgid "Wi-Fi networks"
#~ msgstr "Kablosuz ağlar"

#~ msgid "Martin Kacej"
#~ msgstr "Martin Kacej"
